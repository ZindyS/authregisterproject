package com.example.authorizationproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onRegisterClicked(v: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    fun onLoginClicked(v: View) {
        if (!TextUtils.isEmpty(editTextUsername.text) && !TextUtils.isEmpty(editTextPassword.text)) {
            val retrofit = Retrofit.Builder()
                .baseUrl("http://cars.areas.su")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val api = retrofit.create(SomeApi::class.java)
            api.logout(editTextUsername.text.toString()).enqueue(object : Callback<Data> {
                override fun onResponse(call: Call<Data>?, response: Response<Data>?) {
                    if (response!!.isSuccessful) {
                        if (!TextUtils.isEmpty(response!!.body().notice.text)) {
                            api.auth(editTextUsername.text.toString(), editTextPassword.text.toString()).enqueue(object: Callback<Data> {
                                override fun onResponse(call: Call<Data>?, response: Response<Data>?) {
                                    if (response!!.isSuccessful && response!!.body().notice.token != null && response!!.body().notice.token != 0) {
                                        startActivity(Intent(this@MainActivity, MainActivity2::class.java))
                                    } else {
                                        Toast.makeText(this@MainActivity, "Ошибка: неправильное имя пользователя или пароль", Toast.LENGTH_SHORT).show()
                                    }
                                }

                                override fun onFailure(call: Call<Data>?, t: Throwable?) {
                                    Toast.makeText(this@MainActivity, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                                }

                            })
                        } else {
                            Toast.makeText(this@MainActivity, "Ошибка: неправильное имя пользователя или пароль", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this@MainActivity, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<Data>?, t: Throwable?) {
                    Toast.makeText(this@MainActivity, "Ошибка: ${t!!.message}", Toast.LENGTH_SHORT).show()
                }

            })
        } else {
            Toast.makeText(this, "Проверьте поля!", Toast.LENGTH_SHORT).show()
        }
    }
}