package com.example.authorizationproject

import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface SomeApi {
    @POST("http://cars.areas.su/login")
    fun auth(@Query("username") name: String, @Query("password") password: String) : Call<Data>

    @POST("http://cars.areas.su/logout")
    fun logout(@Query("username") name: String) : Call<Data>

    @POST("http://cars.areas.su/signup")
    fun register(@Query("username") name: String, @Query("password") password: String, @Query("email") email: String) : Call<Data>
}