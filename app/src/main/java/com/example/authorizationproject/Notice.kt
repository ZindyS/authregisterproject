package com.example.authorizationproject

data class Notice(
    val answer: String,
    val token: Int,
    val text: String
)