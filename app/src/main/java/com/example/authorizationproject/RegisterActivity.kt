package com.example.authorizationproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.editTextPassword
import kotlinx.android.synthetic.main.activity_main.editTextUsername
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun onRegisterClicked(v: View) {
        if (!TextUtils.isEmpty(editTextUsername.text) && !TextUtils.isEmpty(editTextPassword.text)) {
            if (editTextPassword.text.toString() == editTextPassword2.text.toString()) {
                var first = false
                for (i in editTextEmail.text.toString()) {
                    if (!first && i=='@') {
                        first = true
                    } else if (i in 'a'..'z') { }
                    else if (i!='.') {
                        first = false
                        break
                    }
                }
                if (first) {
                    val retrofit = Retrofit.Builder()
                        .baseUrl("http://cars.areas.su")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()

                    val api = retrofit.create(SomeApi::class.java)

                    api.register(editTextUsername.text.toString(), editTextPassword.text.toString(), editTextEmail.text.toString()).enqueue(object: Callback<Data> {
                        override fun onResponse(call: Call<Data>?, response: Response<Data>?) {
                            if (response!!.isSuccessful) {
                                startActivity(Intent(this@RegisterActivity, MainActivity2::class.java))
                            } else {
                                Toast.makeText(this@RegisterActivity, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<Data>?, t: Throwable?) {
                            Toast.makeText(this@RegisterActivity, "Ошибка: ${t!!.message}", Toast.LENGTH_SHORT).show()
                        }

                    })
                } else {
                    Toast.makeText(this, "Проверьте правильность Email'а", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "Пароли должны совпадать!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Проверьте поля!", Toast.LENGTH_SHORT).show()
        }
    }

    fun onLoginClicked(v: View) {
        startActivity(Intent(this, MainActivity::class.java))
    }
}